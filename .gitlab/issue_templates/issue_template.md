----------------------------- Delete below -----------------------------

INSTRUCTIONS
============

Before submitting a new issue, please follow the checklist and try to find the answer.

- [ ] I have read the documentation [EmMate Documentation](https://mig.iquesters.com/?s=embedded&p=documentation#) and the issue is not addressed there.
- [ ] I have updated my EmMate branch (master or release) to the latest version and checked that the issue is present there.
- [ ] I have searched the issue tracker for a similar issue and not found a similar issue.

If the issue cannot be solved after the steps before, please follow these instructions so we can get the needed information to help you in a quick and effective fashion.

1. State your **Environment**
2. Describe your problem.
3. Include Debug Logs
4. Provide more items under **Other items if possible** can help us better locate your problem.
5. Use markup (buttons above) and the Preview tab to check what the issue will look like.
6. Delete these instructions from the above to the below marker lines before submitting this issue.

----------------------------- Delete above -----------------------------

## Environment

- Host Machine Details and Versions
- EmMate version
- Hardware Details

## Problem Description

//Detailed problem description goes here.

### Expected Behavior

### Actual Behavior

### Steps to repropduce

1. step1
2. ...

// It helps if you attach a picture of your setup/wiring here.


### Code to reproduce this issue

```cpp
// the code should be wrapped in the ```cpp tag so that it will be displayed better.
#include "abcd.h"

void my_func()
{
    
}

```

## Debug Logs

```
Debug log goes here, should contain the backtrace, as well as the reset source if it is a crash.
Please copy the plain text here for us to search the error log. Or attach the complete logs but leave the main part here if the log is *too* long.
```

## Other items if possible

- [ ] emmate_config file (attach the emmate_config file from your project folder)