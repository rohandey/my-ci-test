#include <stdio.h>

int main() {
    printf("Hello World!!!!\r\n");
    
    return 0;

    /* Regex Test:
     *
     * (issue: #)\d+$
     *
     * Fixes 123
     * Issue: #123
     * 123456789 (?<=\s|^)\d+(?=\s|$)
     * issue: #1
     * ISSUE: #45
     *
     * */
}
